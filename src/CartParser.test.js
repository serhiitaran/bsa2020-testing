import CartParser from "./CartParser";
import fs from "fs";

jest.mock("uuid/v4", () => {
  return () => "id";
});

jest.mock("fs");

let parser, parse, validate, calcTotal, parseLine;

beforeEach(() => {
  parser = new CartParser();
  validate = parser.validate.bind(parser);
  parseLine = parser.parseLine.bind(parser);
  calcTotal = parser.calcTotal;
  parse = parser.parse.bind(parser);
});

describe("CartParser - unit tests", () => {
  describe("parse", () => {
    it("should console.error and return error, when data is not valid", () => {
      const data = "Product,Price,Quantity";
      fs.readFileSync.mockReturnValue(data);
      console.error = jest.fn();

      expect(() => parse()).toThrow();
      expect(console.error).toHaveBeenCalledTimes(1);
    });
  });

  describe("validate", () => {
    it("should return empty array, when received data is valid", () => {
      const data = `Product name,Price,Quantity
			Mollis consequat,9.00,2`;
      const result = [];

      expect(validate(data)).toEqual(result);
    });

    it("should return error, when received wrong header", () => {
      const data = `Product,Price,Quantity
			Mollis consequat,9.00,2`;
      const result = [
        {
          column: 0,
          message:
            'Expected header to be named "Product name" but received Product.',
          row: 0,
          type: "header"
        }
      ];

      expect(validate(data)).toEqual(result);
    });

    it("should return error, when received wrong row cells length", () => {
      const data = `Product name,Price,Quantity
			Mollis consequat,9.00`;
      const result = [
        {
          column: -1,
          message: `Expected row to have 3 cells but received 2.`,
          row: 1,
          type: "row"
        }
      ];

      expect(validate(data)).toEqual(result);
    });

    it("should return error, when received cell is empty string", () => {
      const data = `Product name,Price,Quantity
			 ,9.00,2`;
      const result = [
        {
          column: 0,
          message: 'Expected cell to be a nonempty string but received "".',
          row: 1,
          type: "cell"
        }
      ];
      expect(validate(data)).toEqual(result);
    });

    it("should return error, when received cell value type is not a number", () => {
      const getData = value => `Product name,Price,Quantity
			Mollis consequat,9.00,${value}`;
      const getResult = value => [
        {
          column: 2,
          message: `Expected cell to be a positive number but received "${value}".`,
          row: 1,
          type: "cell"
        }
      ];

      expect(validate(getData("two"))).toEqual(getResult("two"));
      expect(validate(getData(null))).toEqual(getResult(null));
      expect(validate(getData(undefined))).toEqual(getResult(undefined));
      expect(validate(getData(false))).toEqual(getResult(false));
      expect(validate(getData({}))).toEqual(getResult({}));
      expect(validate(getData(Symbol))).toEqual(getResult(Symbol));
    });

    it("should return error, when received cell value is NaN", () => {
      const data = `Product name,Price,Quantity
			Mollis consequat,9.00,NaN`;
      const result = [
        {
          column: 2,
          message: `Expected cell to be a positive number but received "NaN".`,
          row: 1,
          type: "cell"
        }
      ];

      expect(validate(data)).toEqual(result);
    });

    it("should return error, when received cell value is negative number", () => {
      const data = `Product name,Price,Quantity
			Mollis consequat,9.00,-2`;
      const result = [
        {
          column: 2,
          message: `Expected cell to be a positive number but received "-2".`,
          row: 1,
          type: "cell"
        }
      ];

      expect(validate(data)).toEqual(result);
    });
  });

  describe("parseLine", () => {
    it("should return an object with keys from column keys and values from CSV", () => {
      const data = "Mollis consequat,9.00,2";
      const result = {
        id: "id",
        name: "Mollis consequat",
        price: 9.0,
        quantity: 2
      };

      expect(parseLine(data)).toEqual(result);
    });
  });

  describe("calcTotal", () => {
    it("should return correct total price", () => {
      const data = [
        { price: 9.0, quantity: 2 },
        { price: 10.32, quantity: 1 },
        { price: 28.72, quantity: 10 }
      ];
      expect(calcTotal(data)).toBeCloseTo(315.52);
    });
  });

  describe("createError", () => {
    it("should create and return error object", () => {
      const data = [
        "row",
        1,
        -1,
        "Expected row to have 3 cells but received 2."
      ];

      const result = {
        type: "row",
        row: 1,
        column: -1,
        message: "Expected row to have 3 cells but received 2."
      };

      expect(parser.createError(...data)).toEqual(result);
    });
  });
});

describe("CartParser - integration test", () => {
  it("should return a JSON object with cart items and total price", () => {
    const data = `Product name,Price,Quantity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90,1
		Consectetur adipiscing,28.72,10
		Condimentum aliquet,13.90,1	`;

    const result = {
      items: [
        {
          id: "id",
          name: "Mollis consequat",
          price: 9,
          quantity: 2
        },
        {
          id: "id",
          name: "Tvoluptatem",
          price: 10.32,
          quantity: 1
        },
        {
          id: "id",
          name: "Scelerisque lacinia",
          price: 18.9,
          quantity: 1
        },
        {
          id: "id",
          name: "Consectetur adipiscing",
          price: 28.72,
          quantity: 10
        },
        {
          id: "id",
          name: "Condimentum aliquet",
          price: 13.9,
          quantity: 1
        }
      ],
      total: 348.32
    };

    fs.readFileSync.mockReturnValue(data);

    expect(parse(data)).toEqual(result);
  });
});
